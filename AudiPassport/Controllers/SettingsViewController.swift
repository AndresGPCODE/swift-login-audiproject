//
//  SettingsViewController.swift
//  AudiPassport
//
//  Created by Usuario on 1/21/20.
//  Copyright © 2020 Andres. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    @IBOutlet weak var logOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        logOutButton.layer.borderWidth = 2.0
//        logOutButton.layer.borderColor = UIColor.black.cgColor
//        logOutButton.layer.cornerRadius = 8
        // Do any additional setup after loading the view.
//        let icon = UIImage(named: "log_out.png")!
//        logOutButton.setImage(icon, for: .normal)
//        logOutButton.imageView?.contentMode = .scaleAspectFit
//        logOutButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -30, bottom: 0, right: 0)
    }
    
    @IBAction func logOutSys(_ sender: UIButton) {
        print("Cerraste session")
        let LoginViewController = storyboard?.instantiateViewController(withIdentifier: "LoginID") as? LoginViewController
        view.window?.rootViewController = LoginViewController
        view.window?.makeKeyAndVisible()
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
