//  LoginViewController.swift
//  AudiPassport
//  Created by Usuario on 1/22/20.
//  Copyright © 2020 Andres. All rights reserved.

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var logIn: UIButton!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var userText: UITextField!
    @IBOutlet weak var passText: UITextField!
    var ScreenW:Double = 0.0
    var ScreenH:Double = 0.0
    var constraintValues = [NSLayoutConstraint]()
    var constraintValues2 = [NSLayoutConstraint]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        logIn.layer.cornerRadius = 8
        
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -280)
        button.setTitle("Iniciar Sesion", for: .normal)
        
        let bounds = UIScreen.main.bounds
        ScreenW = Double(bounds.size.width)
        ScreenH = Double(bounds.size.height)
        
        if ScreenW > 500 {
            // Dimensionamiento de los textfield del login cunado son pantallas muy grandes
//            print("mayor a 500 pt o px")
            print(ScreenW)
            userText.translatesAutoresizingMaskIntoConstraints = false
            let leadingConstraint = userText.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant:  CGFloat(ScreenW * 0.25))
            let trailingConstraint = userText.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: (CGFloat(ScreenW * 0.25) * -1))
//          let topConstraint = userText.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0)
//          let bottomConstraint = userText.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
//          constraintValues.append(contentsOf: [leadingConstraint,trailingConstraint,topConstraint, bottomConstraint])
            constraintValues.append(contentsOf: [leadingConstraint,trailingConstraint])
            NSLayoutConstraint.activate(constraintValues)
            
            passText.translatesAutoresizingMaskIntoConstraints = false
            let leadingConstraint2 = passText.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant:  CGFloat(ScreenW * 0.25))
            let trailingConstraint2 = passText.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: (CGFloat(ScreenW * 0.25) * -1))
            //          let topConstraint = passText.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0)
            //          let bottomConstraint = passText.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
            //          constraintValues.append(contentsOf: [leadingConstraint,trailingConstraint,topConstraint, bottomConstraint])
            constraintValues2.append(contentsOf: [leadingConstraint2,trailingConstraint2])
            NSLayoutConstraint.activate(constraintValues2)

        }else{
            // Dimensionamiento de los textfild cuando la pantalla es pequeña
//            print("menor a 500 pt o px")
        }
        
    }
    
    @IBAction func loginAccess(_ sender: UIButton) {
        if userText.text! == "" || passText.text! == ""{
            // Mensaje de alerta por error de acceso al login
            alertError(msg: "Por favor completa todos los campos")
        }else{
            //        print("Login function")
            //        print("Acceso al sistema")
            struct ToDoResponseModel: Codable {
                var userId: Int
                var id: Int?
                var email: String
                var password: String
                var resp: String
                var status: Bool
            }
            let direccion = Url.Api() // Instanciamos la estructura de las constantes, en este caso para las direcciones de la API y los archivos PHP.
            let url = URL(string: "\(direccion.ApiUrl)login.php") //  Codificamos el String como una direccion URL vaida para la peticion.
            // Guard guarda crea una variable si se cupe una condicion, de lo contrario ejecuta un else.
            guard let requestUrl = url else { fatalError() }
            var request = URLRequest(url: requestUrl) // configuramos el url como una URL Request valida.
            request.httpMethod = "POST" // Declaramos el metodo del request
            request.setValue("application/json", forHTTPHeaderField: "Accept") // Configuramos un header
            request.setValue("application/json", forHTTPHeaderField: "Content-Type") // Configuramos un header
            let newTodoItem = ToDoResponseModel(userId: 300, id: nil, email: userText.text!, password: passText.text!, resp: "", status: false) // Asignamos los valores a enviar en la peticion como parametros.
            do{
                let jsonData = try JSONEncoder().encode(newTodoItem) // Codificamos la informacion como un objeto JSON de datos para su envio.
                request.httpBody = jsonData // Asignamos el objetos JSON de datos al cuerpo de la peticion.
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in //
                    if let error = error {
                        print("Error took place \(error)")
                        return
                    }
                    guard let data = data else {return}
                    do{
                        // se decodifica el formato json de la respuesta del server en base a todoresponsemodel y sus variables
                        let todoItemModel = try JSONDecoder().decode(ToDoResponseModel.self, from: data)
                        print("Response data: \(todoItemModel)")
                        print("todoItemModel Usuario: \(todoItemModel.email)")
                        print("todoItemModel Password: \(todoItemModel.password)")
                        if todoItemModel.status == true {
                            DispatchQueue.main.async{
                                self.transitionToHome()
                            }
                        } else {
                            DispatchQueue.main.async{
                                self.alertError(msg: "\(todoItemModel.resp)")
                            }
                        }
                    }catch let jsonErr{
                        print(jsonErr)
                    }
                }
                task.resume()
            } catch let errorJson{
                print(errorJson)
                alertError(msg: "No se puede procesar los datos")
            }
        }// fin del else de validacion de los campos
    }// fin de la funcion de loginAccess
    
    func transitionToHome(){
        let ruta = Ruta.Storyboards()
        let HomeViewController = storyboard?.instantiateViewController(withIdentifier: ruta.homeViewController) as? TabBarViewController
        view.window?.rootViewController = HomeViewController
        view.window?.makeKeyAndVisible()
    }
    
    func alertError(msg:String){
        // Create the action buttons for the alert.
        //        let cancelAction = UIAlertAction(title: "No acepto",
        //                                         style: .destructive) { (action) in
        //                                            // Respond to user selection of the action.
        //        }
        let defaultAction = UIAlertAction(title: "Cerrar",
                                          style: .default) { (action) in
                                            // Respond to user selection of the action.
        }
        // Create and configure the alert controller.
        let alert = UIAlertController(title: "Error de acceso!!",
                                      message: "\(msg).",
                                      preferredStyle: .alert)
        alert.addAction(defaultAction)
        //        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
}
