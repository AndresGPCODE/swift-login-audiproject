//
//  PreviewViewController.swift
//  AudiPassport
//
//  Created by Usuario on 1/23/20.
//  Copyright © 2020 Andres. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {
    @IBOutlet weak var btn_login: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        btn_login.layer.cornerRadius = 8
        btn_login.round()
        btn_login.clipsToBounds = true
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
