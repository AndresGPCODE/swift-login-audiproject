//
//  UIButtonExtension.swift
//  AudiPassport
//
//  Created by Usuario on 1/23/20.
//  Copyright © 2020 Andres. All rights reserved.
//

import UIKit

extension UIButton {
    // redondear elemento en este caso boton
    func round(){
        layer.cornerRadius = (bounds.height / 2)
        clipsToBounds = true
    }
    
    // Brillo
    func shine(){
        UIView.animate(withDuration: 0.1, animations: {
            self.alpha = 0.5
        }){ (completation) in
            UIView.animate(withDuration: 0.1, animations: {
                self.alpha = 1
            })
        }
    }
}
