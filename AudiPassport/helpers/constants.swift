//
//  constants.swift
//  AudiPassport
//
//  Created by Usuario on 1/22/20.
//  Copyright © 2020 Andres. All rights reserved.

import Foundation
import UIKit

struct Ruta {
    struct Storyboards {
        let homeViewController:String = "HomeCV"
        let homeViewControllerRef:String = "HomeRef"
        let loginViewController:String = "LoginCV"
    }
}

struct Url {
    struct Api {
        // Direccion de la API
        let ApiUrl = "http://192.168.64.3/_API/"
    }
}


struct Screen {
    struct Dimensions {
        
       
    }
}
